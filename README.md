# Solar spectra for display as background in the SPICE planning tools

## Rationale

The [SPICE plannning tools](https://git.ias.u-psud.fr/spice/spice_planning) include a plot representing the SW and LW detectors, where the different wavelength windows of a study will be displayed, so that the user gets a sense of each window's position in wavelength and Y.
The user wants to see whether the positions of the windows are correct with respect to the wavelengths of spectral lines emitted by the Sun.
It is therefore useful to display spectra (or detector images) as background in the plots representing the windows on the detector.

## Plots for different reference spectra

### Curdt et al. (2001) spectral atlas

`plot_curdt01.py` produces SPICE detector images with intensities as a function of wavelength deduced from the [Curdt et al. (2001)](https://ui.adsabs.harvard.edu/#abs/2001A&A...375..591C/abstract) spectral atlas, obtained from SoHO/SUMER.
The full atlas (continuous spectrum) is downloaded from the MPS web server, and the line identifications are downloaded from the CDS/Vizier table associated to the paper.
This atlas is available for `spot` (sunspot, active region), `ch` (coronal hole) and `qs` (quiet Sun).

A detector image is produced for each detector and each solar region.
No dependence of the intensity in Y is included in the result.
Annotations (pixel and wavelength axes, line identifications) are optional.

Wavelength calibration is assumed to be linear, with values coming from SPICE-RAL-RP-0002.
Intensity calibration is not taken into account (it is assumed to be uniform in each detector, and intensities are normalized and displayed in log scale).

Second-order lines seen by SUMER are displayed, but with potentially large calibration errors.


### Other spectra

To be written if needed.


## Licence

[WTFPL](http://www.wtfpl.net/).
