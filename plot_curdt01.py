#!/usr/bin/env python

'''
Plot SPICE detector images using the Curdt 2001 SUMER spectral atlas full profiles.
This does not take into account (yet):
* the differences in PSF
* the SPICE effective area as a function of wavelength
* the different sensitivity of SUMER and SPICE at the wavelengths of specific lines in 2nd-order
'''

import numpy as np
import matplotlib.pyplot as plt
from scipy.io.idl import readsav
from PIL import Image, ImageDraw, ImageFont
import os
import requests
import json
from astropy import units as u
from matplotlib import cm

# Profiles downloaded from
# http://www.mps.mpg.de/homes/curdt/qs_profiles.xdr
# referenced in
# http://www2.mps.mpg.de/homes/curdt/
# see also
# http://www2.mps.mpg.de/projects/soho/sumer/FILE/Atlas.html
url = 'http://www.mps.mpg.de/homes/curdt/qs_profiles.xdr'
filename = 'curdt01_profiles.sav'
if not os.path.exists(filename):
    with open(filename, 'wb') as f:
        response = requests.get(url)
        f.write(response.content)
s = readsav('curdt01_profiles.sav')

# Spectral ranges (nm), from SPICE-RAL-RP-0002, 01 Oct 2015 7.0
lam = {'SW': (70.387, 79.019),
       'LW': (97.254, 104.925)}

# Size of detectors (x,y), for both SW and LW
size = (1024, 1024)

# Solar regions
regions = ['qs', 'ch', 'spot']

annotate = False

# prepare font for annotations
if annotate:
    fontpath = ['/usr/share/fonts/truetype/droid/',
                '/home/ebuchlin/tmp/',
                '/usr/share/texlive/texmf-dist/fonts/truetype/public/droid/']
    fontname = 'DroidSans-Bold.ttf'
    fontfile = ''
    for fp in fontpath:
        if os.path.exists(fp + fontname):
            fontfile = fp + fontname
            break
    assert(fontfile != '')
    fontsize = 12
    font = ImageFont.truetype(fontfile, fontsize)
    twidth, theight = font.getsize("0")



# prepare spectral atlas (list of lines) for annotations
def getCurdt01Atlas():
    try:
        from astroquery.vizier import Vizier
    except ImportError:
        warnings.warn('astroquery.vizier module not available: cannot get Curdt et al. 2001 line list')
        return []
    Vizier.ROW_LIMIT = -1
    try:
        table = Vizier.get_catalogs('J/A+A/375/591')['J/A+A/375/591/tablea1']
    except (Exception, reason):
        warnings.warn('Could not get Curdt et al. 2001 line list: ' + reason)
        return []
    return table


atlas = getCurdt01Atlas()


for det in lam.keys():
    # Wavelength (x) axis in detector
    detx = np.linspace(lam[det][0], lam[det][1], size[0])
    for region in regions:
        # get relevant profile (with conversion from angström to nm)
        datax = s[region + '_lambda'] / 10
        dataprofile = s[region + '_profil']
        profile = np.interp(detx, datax, dataprofile)
        # normalized and rescaled profile, transformed to image
        profile = np.log(profile)
        mi = np.nanmin(profile)
        ma = np.nanmax(profile)
        profile[np.isnan(profile)] = mi
        profile = (profile - mi) / (ma - mi)
        # apply color map and create image
        rgba = (cm.magma(profile) * 255.99).astype(np.uint8)
        im = rgba[np.newaxis, :, :] * np.ones((size[1], 1, 1), dtype=np.uint8)
        Im = Image.fromarray(im, 'RGBA')

        if annotate:  # optional: annotations
            draw = ImageDraw.Draw(Im, 'RGBA')

            # wavelengths
            for xval in np.arange(np.ceil(detx[0]), detx[-1], dtype=np.int):
                x = np.interp(xval, detx, np.arange(size[0]))
                t = str(xval)
                tw = font.getsize(t)[0]
                draw.text((x - tw/2, 0), t, font=font, fill=(255,255,255)) # align='center' has no effect

            # pixels
            for xval in np.arange(0, size[0], 100):
                t = str(xval)
                tw = font.getsize(t)[0]
                draw.text((xval - tw/2, size[1] - theight), t, font=font, fill=(255,255,255))

            # lines in spectral atlas
            rr = {'qs': 'LQS', 'ch': 'LCH', 'spot': 'LSS'}[region]
            wh = np.all([atlas['Lambda'] > lam[det][0] * 10, atlas['Lambda'] < lam[det][1] * 10], axis=0)
            emax = atlas[wh][rr].max()
            for r in atlas[wh]:
                if not r[rr]: continue  # line not detected
                wvl = r['Lambda'] / 10  # convert angström to nm
                if (wvl >  lam[det][0] and wvl < lam[det][1]):
                    order = ''      # first-order line
                elif (2. * wvl > lam[det][0] and 2. * wvl < lam[det][1]):
                    order = ' (2)'  # second-order line
                else:
                    continue        # wavelength not in plot
                x = np.interp(wvl, detx, np.arange(size[0]))
                # peak spectral radiance determines y position of annotation
                y = 912 - (r[rr] / emax) ** (1/4) * 800
                t = str(r['Line']) + order
                tw = font.getsize(t)[0]
                draw.text((x - tw/2, y), t, font=font, fill=(255,255,255))

        fnbase = 'bg_curdt01_{}_{}'.format(region, det)
        Im.save(fnbase + '.png')

        # save corresponding data, in physical units
        # (with some margin in wavelength)
        w = (datax >= lam[det][0] - 0.1) & (datax <= lam[det][1] + 0.1)
        with open(fnbase + '.json', 'w') as outfile:
            json.dump(
                {
                    'region': region,
                    'detector': det,
                    'datasource': 'Curdt et al. (2001)',
                    'lambda': {
                        'unit': 'angstrom',
                        'values': (datax[w] * 10).tolist(),
                    },
                    'profile': {
                        'unit': 'mW/sr/m^2/A',
                        'values': dataprofile[w].tolist()
                    }
                },
                outfile)
